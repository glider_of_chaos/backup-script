import os
import shutil
import datetime
import time
import zipfile
import zlib


#list of 'end of name' strings to block from copying
lblockedendstrings = ['.bak', '.dwl', 'dwl2', 'tmp', 'cdc']

#list of 'end of name' strings to block from copying
lblockedstartstrings = ['~$']

#folder to back up from
backupfrompath = "d:/sw"
backuptopath = "c:/Backup/"
additionalcopypath = "//NWENCO-32/Reserve/Backup/"

#lets get a list of files out of backuppath directory
#def build_file_list(path, flist=[]):									<<<< WTF??????? doesn't reset flist (duuno why =[] doesn't)
def build_file_list(path, flist = None):
	if flist == None:													# is '==' ok?
		flist=[]
	for dname in os.listdir(path):
		child_path1=os.path.join(path, dname)
		if os.path.isdir(child_path1):
			build_file_list(child_path1, flist)
		else:
			flist.append(child_path1)
	return flist

def last_change_date(cpath):
	print("Geting last change date from  -backup to- folder")
#	print(cpath)
	return max([os.path.getmtime(f) for f in cpath])

#building filelist where time changes are after t1
def filter_list_by_time(f1, t1):
	print("Filetring filelsit by time")
	f2 = [p for p in f1 if os.path.getmtime(p) > t1]
	return f2

def filter_list_by_beginning(f1, lb1):
	print("Removing files starting with ")
	print(lb1)
	f2=[]
	for s1 in f1:
		f2.append(s1)
		for s2 in lb1:
			if s2 == os.path.basename(s1)[:len(s2)]:
				f2.remove(s1)
				break
	return f2

def filter_list_by_ending(f1, le1):
	print("Removing files ending with ")
	print(le1)
	f2=[]
	for s1 in f1:
		f2.append(s1)
		for s2 in le1:
			if s2 == s1[-len(s2):]:
				f2.remove(s1)
				break
	return f2

def run_backup(flist, bpath, cpath):
	ti = time.strftime("%Y-%m-%d_%H-%M-%S.zip")
	sn = bpath + ti
	cn = cpath + ti
	zb = zipfile.ZipFile(sn, mode='w')
	print("Launching backup process to " + sn)
	for p in flist:
		zb.write(p, compress_type=zipfile.ZIP_DEFLATED)
	print("Making additinal copy to " + cn)
	try:
		shutil.copyfile(sn,cn)
	except:
		print("...")
		print("...")
		print("Something went wrong with the copy....")
#		input("Press Enter to continue...")
#		exit()

# actual start
try:
	fplist = build_file_list(backupfrompath)
except:
	print("Sorry, directory you want to backup doesn't seem to exist...")
	input("Press Enter to continue...")
	exit()
try:
	bplist = build_file_list(backuptopath)
except:
	os.makedirs(backuptopath)
	bplist = build_file_list(backuptopath)
if len(bplist) == 0:
	lastbaktime = 0
else:
	lastbaktime = last_change_date(bplist)
fplist2 = filter_list_by_time(fplist, lastbaktime)
fplist3 = filter_list_by_beginning(fplist2, lblockedstartstrings)
fplist4 = filter_list_by_ending(fplist3, lblockedendstrings)
if len(fplist4) == 0:
	print("Seems like everything's up to date already")
else:
	run_backup(fplist4, backuptopath, additionalcopypath)
input("Press Enter to continue...")